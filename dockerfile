FROM node:18-alpine3.15 AS build-img

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN npm install && npm run build

FROM  node:18-alpine3.15 as run-img

WORKDIR /usr/src/app/

COPY --from=build-img /usr/src/app/dist /usr/src/app/dist
COPY --from=build-img /usr/src/app/package*.json ./
COPY --from=build-img /usr/src/app/node_modules /usr/src/app/node_modules 


EXPOSE 3000

CMD ["npm", "run", "start:prod"]
